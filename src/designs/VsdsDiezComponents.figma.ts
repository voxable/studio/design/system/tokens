/**
 * This code was generated by Diez version 10.3.0.
 * Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
 */
import { Color, DropShadow, File, Font, Image, Point2D, Typograph } from "@diez/prefabs";

const vsdsDiezComponentsColors = {
    warm95: Color.rgba(246, 240, 238, 1),
    warm951: Color.rgba(246, 240, 238, 1),
    system360: Color.rgba(191, 0, 0, 1),
    system3601: Color.rgba(191, 0, 0, 1),
    system3602: Color.rgba(191, 0, 0, 1),
    system3603: Color.rgba(191, 0, 0, 1),
    whiteWhite: Color.rgba(255, 255, 255, 1),
    mainDefault: Color.rgba(39, 40, 51, 1),
    indigo11: Color.rgba(16, 4, 52, 1),
    system290: Color.rgba(189, 16, 224, 1),
    system340: Color.rgba(224, 16, 85, 1),
    system324: Color.rgba(255, 92, 190, 1),
    system186: Color.rgba(0, 188, 209, 1),
    system286: Color.rgba(147, 0, 191, 1),
    system255: Color.rgba(48, 0, 191, 1),
    system210: Color.rgba(0, 112, 224, 1),
    system195: Color.rgba(0, 172, 229, 1),
    system161: Color.rgba(0, 153, 105, 1),
    system127: Color.rgba(0, 184, 21, 1),
    system80: Color.rgba(95, 143, 0, 1),
    system35: Color.rgba(194, 113, 0, 1),
    system28: Color.rgba(245, 114, 0, 1),
    system60: Color.rgba(219, 219, 0, 1),
    system53: Color.rgba(255, 226, 5, 1),
    aqua20: Color.rgba(0, 102, 102, 1),
    aqua40: Color.rgba(20, 184, 184, 1),
    aqua50: Color.rgba(25, 230, 216, 1),
    aqua70: Color.rgba(117, 240, 228, 1),
    aqua90: Color.rgba(204, 255, 250, 1),
    aqua95: Color.rgba(229, 255, 253, 1),
    gold30: Color.rgba(153, 107, 0, 1),
    gold40: Color.rgba(204, 143, 0, 1),
    gold50: Color.rgba(255, 187, 0, 1),
    gold70: Color.rgba(255, 214, 102, 1),
    gold80: Color.rgba(254, 227, 154, 1),
    gold90: Color.rgba(255, 241, 204, 1),
    indigo40: Color.rgba(58, 45, 159, 1),
    indigo50: Color.rgba(85, 60, 195, 1),
    indigo95: Color.rgba(234, 229, 255, 1),
    indigo80: Color.rgba(172, 153, 255, 1),
    indigo15: Color.rgba(25, 4, 74, 1),
    warm90: Color.rgba(237, 226, 222, 1),
    whiteWhite1: Color.rgba(255, 255, 255, 1),
    warm60: Color.rgba(158, 150, 148, 1),
    warm40: Color.rgba(107, 99, 97, 1),
    warm80: Color.rgba(214, 199, 194, 1),
    warm70: Color.rgba(187, 175, 171, 1),
    peach70: Color.rgba(255, 145, 102, 1),
    peach80: Color.rgba(255, 182, 153, 1),
    peach90: Color.rgba(255, 218, 204, 1),
    peach95: Color.rgba(250, 239, 234, 1),
    peach60: Color.rgba(235, 118, 71, 1),
    peach50: Color.rgba(204, 94, 51, 1)
};

const vsdsDiezComponentsShadows = {
    elevation4pt: new DropShadow({ offset: Point2D.make(0, 1), radius: 10, color: Color.rgba(107, 99, 97, 0.20000000298023224) }),
    elevation8pt: new DropShadow({ offset: Point2D.make(0, 4), radius: 5, color: Color.rgba(107, 99, 97, 0.20000000298023224) }),
    elevation16pt: new DropShadow({ offset: Point2D.make(0, 8), radius: 10, color: Color.rgba(107, 99, 97, 0.20000000298023224) }),
    elevation8pt1: new DropShadow({ offset: Point2D.make(0, 4), radius: 5, color: Color.rgba(107, 99, 97, 0.20000000298023224) })
};

export const vsdsDiezComponentsFonts = {
    CooperHewitt: {
        Book: Font.fromFile("assets/VsdsDiezComponents.figma.contents/fonts/CooperHewitt-Book.otf"),
        Semibold: Font.fromFile("assets/VsdsDiezComponents.figma.contents/fonts/CooperHewitt-Semibold.otf")
    },
    IAWriterDuoS: {
        Regular: Font.fromFile("assets/VsdsDiezComponents.figma.contents/fonts/iAWriterDuoS-Regular.ttf")
    }
};

const vsdsDiezComponentsTypography = {
    h2: new Typograph({ letterSpacing: 0.00017959170509129763, fontSize: 32, lineHeight: 37.5, color: Color.rgba(0, 0, 0, 1), font: vsdsDiezComponentsFonts.CooperHewitt.Book }),
    h1: new Typograph({ letterSpacing: 0.00017959170509129763, fontSize: 36, lineHeight: 42.1875, color: Color.rgba(0, 0, 0, 1), font: vsdsDiezComponentsFonts.CooperHewitt.Semibold }),
    pBot18pt: new Typograph({ letterSpacing: 0.00017460310482420027, fontSize: 18, lineHeight: 32, color: Color.rgba(0, 0, 0, 1), font: vsdsDiezComponentsFonts.IAWriterDuoS.Regular }),
    h3: new Typograph({ letterSpacing: 0.00017959170509129763, fontSize: 28, lineHeight: 32.8125, color: Color.rgba(25, 4, 74, 1), font: vsdsDiezComponentsFonts.CooperHewitt.Book }),
    h4: new Typograph({ letterSpacing: 0.00017959170509129763, fontSize: 24, lineHeight: 28.125, color: Color.rgba(25, 4, 74, 1), font: vsdsDiezComponentsFonts.CooperHewitt.Semibold }),
    h5: new Typograph({ letterSpacing: 0.00017959170509129763, fontSize: 24, lineHeight: 28.125, color: Color.rgba(25, 4, 74, 1), font: vsdsDiezComponentsFonts.CooperHewitt.Book }),
    h6: new Typograph({ letterSpacing: 0.00017959170509129763, fontSize: 22, lineHeight: 25.78125, color: Color.rgba(25, 4, 74, 1), font: vsdsDiezComponentsFonts.CooperHewitt.Book }),
    pBot20pt: new Typograph({ letterSpacing: 0.00017460310482420027, fontSize: 20, lineHeight: 27, color: Color.rgba(25, 4, 74, 1), font: vsdsDiezComponentsFonts.IAWriterDuoS.Regular }),
    pUser18pt: new Typograph({ letterSpacing: 0.5001920461654663, fontSize: 18, lineHeight: 32, color: Color.rgba(25, 4, 74, 1), font: vsdsDiezComponentsFonts.CooperHewitt.Book }),
    pBot18pt1: new Typograph({ letterSpacing: 0.00017460310482420027, fontSize: 18, lineHeight: 32, color: Color.rgba(16, 4, 52, 1), font: vsdsDiezComponentsFonts.IAWriterDuoS.Regular })
};

export const vsdsDiezComponentsComponentsFiles = {
    avatarProfileDefault128: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault128.png" }),
    avatarProfileDefault1282x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault128@2x.png" }),
    avatarProfileDefault1283x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault128@3x.png" }),
    avatarProfileDefault1284x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault128@4x.png" }),
    avatarUserDefault128: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault128.png" }),
    avatarUserDefault1282x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault128@2x.png" }),
    avatarUserDefault1283x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault128@3x.png" }),
    avatarUserDefault1284x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault128@4x.png" }),
    avatarUserDefault48: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault48.png" }),
    avatarUserDefault482x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault48@2x.png" }),
    avatarUserDefault483x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault48@3x.png" }),
    avatarUserDefault484x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault48@4x.png" }),
    avatarProfileDefault1281: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1281.png" }),
    avatarProfileDefault12812x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1281@2x.png" }),
    avatarProfileDefault12813x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1281@3x.png" }),
    avatarProfileDefault12814x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1281@4x.png" }),
    avatarUserDefault1281: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault1281.png" }),
    avatarUserDefault12812x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault1281@2x.png" }),
    avatarUserDefault12813x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault1281@3x.png" }),
    avatarUserDefault12814x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault1281@4x.png" }),
    avatarUserDefault481: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault481.png" }),
    avatarUserDefault4812x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault481@2x.png" }),
    avatarUserDefault4813x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault481@3x.png" }),
    avatarUserDefault4814x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault481@4x.png" }),
    avatarUserDefault32: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault32.png" }),
    avatarUserDefault322x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault32@2x.png" }),
    avatarUserDefault323x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault32@3x.png" }),
    avatarUserDefault324x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault32@4x.png" }),
    avatarBotDefault128: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault128.png" }),
    avatarBotDefault1282x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault128@2x.png" }),
    avatarBotDefault1283x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault128@3x.png" }),
    avatarBotDefault1284x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault128@4x.png" }),
    avatarBotDefault48: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault48.png" }),
    avatarBotDefault482x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault48@2x.png" }),
    avatarBotDefault483x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault48@3x.png" }),
    avatarBotDefault484x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault48@4x.png" }),
    avatarProfileDefault1282: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1282.png" }),
    avatarProfileDefault12822x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1282@2x.png" }),
    avatarProfileDefault12823x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1282@3x.png" }),
    avatarProfileDefault12824x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1282@4x.png" }),
    avatarBotDefault1281: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault1281.png" }),
    avatarBotDefault12812x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault1281@2x.png" }),
    avatarBotDefault12813x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault1281@3x.png" }),
    avatarBotDefault12814x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault1281@4x.png" }),
    avatarBotDefault481: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault481.png" }),
    avatarBotDefault4812x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault481@2x.png" }),
    avatarBotDefault4813x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault481@3x.png" }),
    avatarBotDefault4814x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault481@4x.png" }),
    avatarBotDefault32: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault32.png" }),
    avatarBotDefault322x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault32@2x.png" }),
    avatarBotDefault323x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault32@3x.png" }),
    avatarBotDefault324x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault32@4x.png" }),
    avatarProfileDefault48: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault48.png" }),
    avatarProfileDefault482x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault48@2x.png" }),
    avatarProfileDefault483x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault48@3x.png" }),
    avatarProfileDefault484x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault48@4x.png" }),
    avatarProfileDefault1283: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1283.png" }),
    avatarProfileDefault12832x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1283@2x.png" }),
    avatarProfileDefault12833x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1283@3x.png" }),
    avatarProfileDefault12834x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1283@4x.png" }),
    avatarProfileDefault481: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault481.png" }),
    avatarProfileDefault4812x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault481@2x.png" }),
    avatarProfileDefault4813x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault481@3x.png" }),
    avatarProfileDefault4814x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault481@4x.png" }),
    avatarProfileDefault32: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault32.png" }),
    avatarProfileDefault322x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault32@2x.png" }),
    avatarProfileDefault323x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault32@3x.png" }),
    avatarProfileDefault324x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault32@4x.png" }),
    container1ptRounded4pt: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/Container1ptRounded4pt.png" }),
    container1ptRounded4pt2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/Container1ptRounded4pt@2x.png" }),
    container1ptRounded4pt3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/Container1ptRounded4pt@3x.png" }),
    container1ptRounded4pt4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/Container1ptRounded4pt@4x.png" }),
    contextTagReadDefaultMaster: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster.png" }),
    contextTagReadDefaultMaster2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster@2x.png" }),
    contextTagReadDefaultMaster3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster@3x.png" }),
    contextTagReadDefaultMaster4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster@4x.png" }),
    iconBasicExitDefault: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault.png" }),
    iconBasicExitDefault2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault@2x.png" }),
    iconBasicExitDefault3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault@3x.png" }),
    iconBasicExitDefault4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault@4x.png" }),
    contextTagReadDefaultMaster1: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster1.png" }),
    contextTagReadDefaultMaster12x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster1@2x.png" }),
    contextTagReadDefaultMaster13x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster1@3x.png" }),
    contextTagReadDefaultMaster14x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster1@4x.png" }),
    contextTagWriteDefaultMaster: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagWriteDefaultMaster.png" }),
    contextTagWriteDefaultMaster2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagWriteDefaultMaster@2x.png" }),
    contextTagWriteDefaultMaster3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagWriteDefaultMaster@3x.png" }),
    contextTagWriteDefaultMaster4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagWriteDefaultMaster@4x.png" }),
    iconBasicExitDefault1: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault1.png" }),
    iconBasicExitDefault12x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault1@2x.png" }),
    iconBasicExitDefault13x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault1@3x.png" }),
    iconBasicExitDefault14x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault1@4x.png" }),
    contextTagReadDefaultMaster2: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster2.png" }),
    contextTagReadDefaultMaster22x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster2@2x.png" }),
    contextTagReadDefaultMaster23x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster2@3x.png" }),
    contextTagReadDefaultMaster24x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster2@4x.png" }),
    contextTagReadEnd: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadEnd.png" }),
    contextTagReadEnd2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadEnd@2x.png" }),
    contextTagReadEnd3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadEnd@3x.png" }),
    contextTagReadEnd4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadEnd@4x.png" }),
    iconBasicExitDefault2: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault2.png" }),
    iconBasicExitDefault22x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault2@2x.png" }),
    iconBasicExitDefault23x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault2@3x.png" }),
    iconBasicExitDefault24x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault2@4x.png" }),
    contextTagReadDefaultMaster3: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster3.png" }),
    contextTagReadDefaultMaster32x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster3@2x.png" }),
    contextTagReadDefaultMaster33x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster3@3x.png" }),
    contextTagReadDefaultMaster34x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster3@4x.png" }),
    contextTagReadHover: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadHover.png" }),
    contextTagReadHover2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadHover@2x.png" }),
    contextTagReadHover3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadHover@3x.png" }),
    contextTagReadHover4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/ContextTagReadHover@4x.png" }),
    bottomCaret: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/BottomCaret.png" }),
    bottomCaret2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/BottomCaret@2x.png" }),
    bottomCaret3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/BottomCaret@3x.png" }),
    bottomCaret4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/BottomCaret@4x.png" }),
    userMaster: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/UserMaster.png" }),
    userMaster2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/UserMaster@2x.png" }),
    userMaster3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/UserMaster@3x.png" }),
    userMaster4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/UserMaster@4x.png" }),
    bot: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/Bot.png" }),
    bot2x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/Bot@2x.png" }),
    bot3x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/Bot@3x.png" }),
    bot4x: new File({ src: "assets/VsdsDiezComponents.figma.contents/components/Bot@4x.png" })
};

export const vsdsDiezComponentsComponents = {
    avatarProfileDefault128: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault128.png", 128, 128),
    avatarUserDefault128: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault128.png", 128, 128),
    avatarUserDefault48: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault48.png", 48, 48),
    avatarProfileDefault1281: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1281.png", 32, 32),
    avatarUserDefault1281: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault1281.png", 32, 32),
    avatarUserDefault481: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault481.png", 32, 32),
    avatarUserDefault32: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarUserDefault32.png", 32, 32),
    avatarBotDefault128: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault128.png", 128, 128),
    avatarBotDefault48: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault48.png", 48, 48),
    avatarProfileDefault1282: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1282.png", 32, 32),
    avatarBotDefault1281: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault1281.png", 32, 32),
    avatarBotDefault481: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault481.png", 32, 32),
    avatarBotDefault32: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarBotDefault32.png", 32, 32),
    avatarProfileDefault48: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault48.png", 48, 48),
    avatarProfileDefault1283: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault1283.png", 32, 32),
    avatarProfileDefault481: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault481.png", 32, 32),
    avatarProfileDefault32: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/AvatarProfileDefault32.png", 32, 32),
    container1ptRounded4pt: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/Container1ptRounded4pt.png", 288, 288),
    contextTagReadDefaultMaster: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster.png", 215, 24),
    iconBasicExitDefault: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault.png", 8, 8),
    contextTagReadDefaultMaster1: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster1.png", 235, 24),
    contextTagWriteDefaultMaster: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/ContextTagWriteDefaultMaster.png", 235, 24),
    iconBasicExitDefault1: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault1.png", 8, 8),
    contextTagReadDefaultMaster2: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster2.png", 215, 24),
    contextTagReadEnd: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/ContextTagReadEnd.png", 215, 24),
    iconBasicExitDefault2: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/IconBasicExitDefault2.png", 8, 8),
    contextTagReadDefaultMaster3: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/ContextTagReadDefaultMaster3.png", 215, 24),
    contextTagReadHover: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/ContextTagReadHover.png", 215, 24),
    bottomCaret: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/BottomCaret.png", 88, 61.65199279785156),
    userMaster: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/UserMaster.png", 759, 80),
    bot: Image.responsive("assets/VsdsDiezComponents.figma.contents/components/Bot.png", 759, 64)
};

export const vsdsDiezComponentsTokens = {
    colors: vsdsDiezComponentsColors,
    shadows: vsdsDiezComponentsShadows,
    typography: vsdsDiezComponentsTypography
};
