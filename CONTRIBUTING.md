# Contributing

Thanks for contributing to VSDS! The below information will help get you acquainted with the process of contributing to the project.

# Commit message format

This project uses the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) commit message format (if it's good enough for [Vue](https://github.com/vuejs/vue), it's good enough for us). **Please format your commit messages to this standard.** This will help us automatically generate our changelog, among other niceties. 

In general, Conventional Commits follow this format: 

```
type(scope?): subject  # scope is optional

body?                  # body is optional
```
      
Here's an example of a Conventional Commit:

```
fix(components): correct bug in header component
```

We provide [commitizen](https://github.com/commitizen/cz-cli) and [commitlint](https://commitlint.js.org/) as development dependencies. If you'd like an interactive prompt to help format your commit message properly, simply run:

```console
yarn run commit
```

The commit message format will be validated by commitlint with a [husky](https://github.com/typicode/husky) pre-commit hook. **These hooks will only run on the master branch,** so if you're working in a feature branch, feel free to ignore the proper formatting, and then use [interactive rebase or a squash merge](https://thoughtbot.com/blog/git-interactive-rebase-squash-amend-rewriting-history) to create properly-formatted commits before submitting your merge request (or before removing [the "Work in Progress" flag](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html)).

You can also use [Commitizen] when committing to this repo to properly format your commit messages:

```console
git cz
```

## Commit types & scopes

These are the commit types used within this project:

    build
    ci
    chore
    docs
    feat
    fix
    perf
    refactor
    revert
    style
    test
    
And here are the scopes:

    tooling
    diez

# Code of Conduct

This project adheres to [the Contributor Covenant](/CODE_OF_CONDUCT.md). By contributing, you agree to be bound by its terms.


