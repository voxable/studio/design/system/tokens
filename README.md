# Voxable Studio Design System Tokens

These are the [design tokens](https://css-tricks.com/what-are-design-tokens/) used by [the Voxable Studio Design System (VSDS)](https://gitlab.com/voxable/design/docs). This project makes use of [Diez](https://diez.org) for building the design tokens directly from our Figma designs.

## Getting Started

```console
yarn install
yarn build
```

This will download and install all node dependencies, then build a package containing Diez as well as the tokens themselves.

## Updating from Figma

In order to update the tokens from the Figma designs, simply run `yarn extract`.

## Contributing

See [our guide to contributing.](/CONTRIBUTING.md)
